from django.shortcuts import get_object_or_404
from rest_framework import status
from rest_framework.views import APIView
from rest_framework.response import Response

from reservation.models import Reservation, Client, Invoice, Room
from reservation.serializers import ReservationsSerializer, ClientsSerializer, CreateClientSerializer, RoomsSerializer, \
    InvoicesSerializer, CreateInvoiceSerializer


class ClientView(APIView):

    def get(self, request):
        clients = Client.objects.all()
        serializer = ClientsSerializer(clients, many=True)
        return Response(serializer.data)

    def post(self, request):
        serializer = CreateClientSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        client = serializer.save()
        return Response(ClientsSerializer(client).data)

    def put(self, request, pk, format=None):
        client = get_object_or_404(Client, pk=pk)
        serializer = ClientsSerializer(client, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk, format=None):
        client = get_object_or_404(Client, pk=pk)
        client.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


class InvoiceView(APIView):

    def get(self, request):
        invoices = Invoice.objects.all()
        serializer = InvoicesSerializer(invoices, many=True)
        return Response(serializer.data)

    def post(self, request):
        serializer = CreateInvoiceSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        invoice = serializer.save()
        return Response(InvoicesSerializer(invoice).data)

    def put(self, request, pk, format=None):
        invoice = get_object_or_404(Invoice, pk=pk)
        serializer = InvoicesSerializer(invoice, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk, format=None):
        invoice = get_object_or_404(Invoice, pk=pk)
        invoice.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


class RoomView(APIView):

    def get(self, request):
        rooms = Room.objects.all()
        serializer = RoomsSerializer(rooms, many=True)
        return Response(serializer.data)

    def post(self, request):
        serializer = RoomsSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        room = serializer.save()
        return Response(RoomsSerializer(room).data)

    def put(self, request, pk, format=None):
        room = get_object_or_404(Room, pk=pk)
        serializer = RoomsSerializer(room, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk, format=None):
        room = get_object_or_404(Room, pk=pk)
        room.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


class ReservationView(APIView):

    def get(self, request):
        reservations = Reservation.objects.all()
        serializer = ReservationsSerializer(reservations, many=True)
        return Response(serializer.data)

    def post(self, request):
        serializer = ReservationsSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        reservation = serializer.save()
        return Response(ReservationsSerializer(reservation).data)

    def put(self, request, pk, format=None):
        reservation = get_object_or_404(Reservation, pk=pk)
        serializer = ReservationsSerializer(reservation, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk, format=None):
        reservation = get_object_or_404(Reservation, pk=pk)
        reservation.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)
