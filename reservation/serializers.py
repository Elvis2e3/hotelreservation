from rest_framework import serializers
from rest_framework.validators import UniqueValidator

from reservation.models import Reservation, Client, Invoice, Room


# -------------- Client ------------------

class ClientsSerializer(serializers.ModelSerializer):
    """List all clients"""

    class Meta:
        model = Client
        fields = '__all__'


class CreateClientSerializer(serializers.Serializer):
    """Create Client"""

    name = serializers.CharField(max_length=100)
    last_name = serializers.CharField(max_length=100)
    identity_number = serializers.CharField(
        max_length=100,
        validators=[
            UniqueValidator(queryset=Client.objects.all())
        ]
    )
    phone_number = serializers.IntegerField()
    email = serializers.EmailField(required=False)

    def create(self, validated_data):
        return Client.objects.create(**validated_data)


# -------------- Invoice ------------------

class InvoicesSerializer(serializers.ModelSerializer):
    """List all ivoices"""

    class Meta:
        model = Invoice
        fields = '__all__'


class CreateInvoiceSerializer(serializers.Serializer):
    """Create Invoice"""

    name = serializers.CharField(max_length=100)
    number = serializers.CharField(
        max_length=100,
        validators=[
            UniqueValidator(queryset=Invoice.objects.all())
        ]
    )

    def create(self, validated_data):
        return Invoice.objects.create(**validated_data)


# -------------- Room ------------------

class RoomsSerializer(serializers.ModelSerializer):
    """List and create rooms"""

    class Meta:
        model = Room
        fields = '__all__'


# -------------- Reservation ------------------

class ReservationsSerializer(serializers.ModelSerializer):
    """List and create reservations"""

    class Meta:
        model = Reservation
        fields = '__all__'
