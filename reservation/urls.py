from django.urls import path
from reservation import views

urlpatterns = [
    path('clients', views.ClientView.as_view()),
    path('clients/<int:pk>/', views.ClientView.as_view()),
    path('invoices', views.InvoiceView.as_view()),
    path('invoices/<int:pk>/', views.InvoiceView.as_view()),
    path('rooms', views.RoomView.as_view()),
    path('rooms/<int:pk>/', views.RoomView.as_view()),
    path('reservations', views.ReservationView.as_view()),
    path('reservations/<int:pk>/', views.ReservationView.as_view()),
]
