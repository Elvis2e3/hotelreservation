from django.contrib import admin

# Register your models here.
from reservation.models import Client, Invoice, Room, Reservation

admin.site.register(Client)
admin.site.register(Invoice)
admin.site.register(Room)
admin.site.register(Reservation)

