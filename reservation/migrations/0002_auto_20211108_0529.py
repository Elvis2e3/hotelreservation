# Generated by Django 3.2.9 on 2021-11-08 05:29

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('reservation', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='client',
            name='identity_number',
            field=models.CharField(max_length=100, unique=True),
        ),
        migrations.AlterField(
            model_name='invoice',
            name='number',
            field=models.CharField(max_length=100, unique=True),
        ),
        migrations.AlterField(
            model_name='room',
            name='number',
            field=models.IntegerField(unique=True),
        ),
    ]
