from django.db import models

# Create your models here.


class Client(models.Model):
    name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    identity_number = models.CharField(max_length=100, unique=True)
    phone_number = models.IntegerField()
    email = models.EmailField(null=True, blank=True)

    def __str__(self):
        return "%s %s" % (self.name, self.last_name)


class Invoice(models.Model):
    name = models.CharField(max_length=100)
    number = models.CharField(max_length=100, unique=True)

    def __str__(self):
        return "%s - %s" % (self.name, self.number)


class Room(models.Model):

    INDIVIDUAL = 1
    DOUBLE = 2
    TRIPLE = 3
    QUEEN = 4
    KING = 5

    CHOICE_ROOM = (
        (INDIVIDUAL, "INDIVIDUAL"),
        (DOUBLE, "DOUBLE"),
        (TRIPLE, "TRIPLE"),
        (QUEEN, "QUEEN"),
        (KING, "KING"),
    )

    number = models.IntegerField(unique=True)
    type = models.IntegerField(choices=CHOICE_ROOM, default=INDIVIDUAL)
    cost = models.IntegerField()

    def __str__(self):
        return str(self.number)


class Reservation(models.Model):

    PENDING = 1
    PAID_OUT = 2
    REMOVED = 3

    CASH = 1
    WIRE_TRANSFER = 2
    DEBIT_CARD = 3
    CREDIT_CARD = 4

    CHOICES_STATUS = (
        (PENDING, "PENDING"),
        (PAID_OUT, "PAID OUT"),
        (REMOVED, "REMOVED"),
    )

    CHOICES_PAYMENT_METHOD = (
        (CASH, "CASH"),
        (WIRE_TRANSFER, "WIRE TRANSFER"),
        (DEBIT_CARD, "DEBIT CARD"),
        (CREDIT_CARD, "CREDIT CARD"),
    )

    client = models.ForeignKey(Client, on_delete=models.CASCADE)
    room = models.ForeignKey(Room, on_delete=models.CASCADE)
    invoice = models.ForeignKey(Invoice, on_delete=models.CASCADE, null=True, blank=True)
    days = models.IntegerField(default=1)
    payment_method = models.IntegerField(choices=CHOICES_PAYMENT_METHOD, default=CASH)
    status = models.IntegerField(choices=CHOICES_STATUS, default=PENDING)

    def __str__(self):
        return str(self.id)
