# Hotel Reservation
## Instalación
* Instalamos los requerimientos del proyecto\
``pip install -r requirements.txt``
* Aplicamos migraciones\
``python manage.py migrate``
* Creamos superusuario para ingresar al admin de django\
``python manage.py createsuperuser``
## Documentación

###Clientes

```
_______________________________________________________________
|   Columna       |    Tipo      |        Atributos            |      
_______________________________________________________________
| name            | CharField    | max_length=100              |
| last_name       | CharField    | max_length=100              |
| identity_number | CharField    | max_length=100, unique=True |
| phone_number    | IntegerField |                             |
| email           | EmailField   | null=True, blank=True       |
\______________________________________________________________/
```
* **/hotelreservation/clients** *[GET, POST, PUT, DELETE]* > Lista, crea, editar y eliminar clientes.
```json
    {
        "id": 1,
        "name": "Elvis",
        "last_name": "Basilio",
        "identity_number": "10674508",
        "phone_number": 75142775,
        "email": "elvis.2e3@gmail.com"
    }
```
---
### Facturacion
```
_______________________________________________________________
|   Columna       |    Tipo      |        Atributos            |      
_______________________________________________________________
| name            | CharField    | max_length=100              |
| number          | CharField    | max_length=100, unique=True |
\______________________________________________________________/
```
* **/hotelreservation/invoices** *[GET, POST, PUT, DELETE]* > Lista, crea, editar y eliminar datos de factura.
```json
  {
        "id": 1,
        "name": "Elvis Edson Basilio Chambi",
        "number": "10674508018"
    }
```
---
### Habitación
```
___________________________________________________________________________
|   Columna       |    Tipo      |        Atributos                        |      
___________________________________________________________________________
| name            | IntegerField | unique=True                             |
| type            | IntegerField | choices=CHOICE_ROOM, default=INDIVIDUAL |
| cost            | IntegerField |                                         |
\__________________________________________________________________________/
    
    |  CHOICE_ROOM
    -----------------
    | INDIVIDUAL = 1
    | DOUBLE = 2
    | TRIPLE = 3
    | QUEEN = 4
    | KING = 5
```
* **/hotelreservation/rooms** *[GET, POST, PUT, DELETE]* > Lista, crea, editar y eliminar habitaciones de hotel.
```json
    {
        "id": 1,
        "number": 1,
        "type": 1,
        "cost": 50
    }
```
---
### Reservación
```
_____________________________________________________________________________________
|   Columna       |    Tipo      |        Atributos                                  |      
_____________________________________________________________________________________
| client          | ForeignKey   | Client, on_delete=CASCADE                         |
| room            | ForeignKey   | Room, on_delete=CASCADE                           |
| invoice         | ForeignKey   | Invoice, on_delete=CASCADE, null=True, blank=True |
| days            | IntegerField | default=1                                         |
| payment_method  | IntegerField | choices=CHOICES_PAYMENT_METHOD, default=CASH      |
| status          | IntegerField | choices=CHOICES_STATUS, default=PENDING           |
\____________________________________________________________________________________/
    
    ---------------------------
    |   CHOICES_STATUS
    ---------------------------
    | PENDING = 1
    | PAID_OUT = 2
    | REMOVED = 3
    
    ----------------------------
    |   CHOICES_PAYMENT_METHOD
    ----------------------------
    | CASH = 1
    | WIRE_TRANSFER = 2
    | DEBIT_CARD = 3
    | CREDIT_CARD = 4

```
* **/hotelreservation/reservations** *[GET, POST, PUT, DELETE]* > Lista, crea, editar y eliminar reservas de habitación.
```json
    {
        "id": 1,
        "days": 2,
        "payment_method": 1,
        "status": 2,
        "client": 1,
        "room": 4,
        "invoice": 1
    }
```
---
* **/hotelreservation/admin** 

## Flujo para la reserva de habitación
1. Primero registraremos las habitaciones que tendra nuestro Hotel.
>    [POST]  /hotelreservation/rooms
2. Una vez tengamos nuestras habitaciones iniciaremos con el registro del cliente:
>    [POST] /hotelreservation/clients   
3. El registro de datos de la Factura no es obligatorio, ya que es posible que el cliente no requiera, tampoco esta ligado directmente al cliente ya que es posible que los datos de factura esten dirigidos a otra persona o entidad.
>    [POST] /hotelreservation/invoices
4. En este punto podemos listar la informacion mediante:
>   [GET] /hotelreservation/rooms\
>   [GET] /hotelreservation/clients\
>   [GET] /hotelreservation/invoices

5. Para el regitro de una Reserva tendremos que cumplir con tres datos: Habitacion, Cliente y Facturacion. En el front se tendra un formulario, en cual es necesario recuperar el ID de la habitacion a reservar, tambien recuperar el ID del cliente y si no siempre es posible realizar un registro para luego recuperar el ID del cliente, opcional pero si es requerido tambien se tendra que recuperar el ID de los datos para la Factura, una vez obtenido esta información se prosigue a registrar lo restante como los dias de estadia, metodo de pago y estado de la reserva.
>   [POST] /hotelreservation/reservations
6. Las reservaciones se pueden cambiar de estado con: 
>   [PUT] /hotelreservation/reservations